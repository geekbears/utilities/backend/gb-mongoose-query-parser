import { suite, test } from '@testdeck/mocha';
import { assert } from 'chai';

import { MongooseQueryParser, QueryParser } from './';
import { FilterQuery, PipelineStage, Types, isObjectIdOrHexString } from 'mongoose';
import { isEqual } from 'lodash';

@suite('Tester')
export class Tester {
    @test('should parse general query')
    generalParse() {
        const parser = new MongooseQueryParser();
        const qry = 'date=2016-01-01&boolean=true&integer=10&regexp=/foobar/i&null=null';
        const parsed: any = parser.parse(qry);
        assert.isNotNull(parsed.filter);
        assert.isOk(parsed.filter.date instanceof Date);
        assert.isOk(parsed.filter.boolean === true);
        assert.isOk(parsed.filter.integer === 10);
        assert.isOk(parsed.filter.regexp instanceof RegExp);
        assert.isOk(parsed.filter.null === null);
    }

    @test('should parse query with string templates')
    generalParse2() {
        const parser = new MongooseQueryParser();
        const predefined = {
            vip: { name: { $in: ['Google', 'Microsoft', 'NodeJs'] } },
            sentStatus: 'sent',
        };
        const parsed: any = parser.parse(
            '${vip}&status=${sentStatus}&timestamp>2017-10-01&author.firstName=/john/i&limit=100&skip=50&sort=-timestamp&select=name&populate=children',
            predefined,
        );
        assert.isOk(parsed.filter.status === predefined.sentStatus);
        assert.isOk(parsed.filter.name.$in.length === 3); // checking parsing of ${vip}
        assert.isOk(parsed.filter.timestamp.$gt instanceof Date);
        assert.isOk(parsed.filter['author.firstName'] instanceof RegExp);
        assert.isOk(parsed.limit === 100);
        assert.isOk(parsed.skip === 50);
        assert.isNotNull(parsed.sort);
        assert.isNotNull(parsed.select);
        assert.isNotNull(parsed.populate);
    }

    @test('should parse populate query')
    populateParse() {
        const parser = new MongooseQueryParser();
        const qry = '_id=1&populate=serviceSalesOrders,customer.category,customer.name';
        const parsed = parser.parse(qry);
        assert.isOk((parsed.populate as any).length === 2);
    }

    @test('should parse built in casters')
    builtInCastersTest() {
        const parser: any = new MongooseQueryParser();
        const qry = 'key1=string(10)&key2=date(2017-10-01)&key3=string(null)';
        const parsed = parser.parse(qry);
        assert.isOk(typeof parsed.filter.key1 === 'string');
        assert.isOk(parsed.filter.key2 instanceof Date);
        assert.isOk(typeof parsed.filter.key3 === 'string');
    }

    @test('should parse custom caster')
    parseCaster() {
        const parser: any = new MongooseQueryParser({
            casters: { $: val => '$' + val },
        });
        const qry = '_id=$(1)';
        const parsed = parser.parse(qry);
        assert.equal('$1', parsed.filter._id);
    }

    @test('should parse json filter')
    parseJsonFilter() {
        const parser = new MongooseQueryParser();
        const obj = {
            $or: [{ key1: 'value1' }, { key2: 'value2' }],
        };
        const qry = `filter=${JSON.stringify(obj)}&name=Google`;
        const parsed: any = parser.parse(qry);
        assert.isArray(parsed.filter.$or);
        assert.isOk(parsed.filter.name === 'Google');
    }

    @test('should parse predefined query objects')
    parsePredefined() {
        const parser = new MongooseQueryParser();
        const preDefined = {
            isActive: { status: { $in: ['In Progress', 'Pending'] } },
            vip: ['KFC', 'Google', 'MS'],
            secret: 'my_secret',
            mykey: 'realkey',
        };
        // test predefined query as key
        let qry = '${isActive}&name&${mykey}=1';
        let parsed: any = parser.parse(qry, preDefined);
        assert.isNotNull(parsed.filter.status);
        assert.isOk(!parsed.filter['${isActive}']);
        assert.isOk(parsed.filter.realkey === 1);
        // test predefined query as value
        qry = 'secret=${secret}';
        parsed = parser.parse(qry, preDefined);
        assert.isOk(parsed.filter.secret === preDefined.secret);
        // test predefined query in json
        qry = 'filter={"$and": ["${isActive}", {"customer": "VDF"}]}';
        parsed = parser.parse(qry, preDefined);
        assert.isNotNull(parsed.filter.$and[0].status);
    }
    @test('should parse lean')
    parseLean() {
        const parser = new MongooseQueryParser();
        const qryTrue = 'lean=true';
        const qryFalse = 'lean=false';
        const qryUndefined = 'lean=';
        const qryNotBoolean = 'lean=any';
        const parsedTrue = parser.parse(qryTrue);
        const parsedFalse = parser.parse(qryFalse);
        const parsedUndefined = parser.parse(qryUndefined);
        const parsedNotBoolean = parser.parse(qryNotBoolean);

        assert.isTrue(parsedTrue.lean);
        assert.isFalse(parsedFalse.lean);
        assert.notNestedProperty(parsedUndefined, 'lean');
        assert.isFalse(parsedNotBoolean.lean);
    }
    @test('should parse complex')
    parseComplex() {
        const parser = new MongooseQueryParser();
        const preDefined = {
            $and: [
                { $or: [{ __keywords: { $elemMatch: { $regex: 'Stranger', $options: 'i' } } }] },
                { dailyRate: { $gte: 0, $lte: 60 } },
                {
                    geolocation: {
                        $near: {
                            $geometry: { type: 'Point', coordinates: [-122.16106, 37.37646] },
                            $minDistance: 0,
                            $maxDistance: 10,
                        },
                    },
                },
                { unavailableDates: { $nin: [] } },
            ],
        };
        const query =
            'filter={"$and":[{"$or":[{"__keywords":{"$elemMatch":{"$regex":"Stranger","$options":"i"}}}]},{"dailyRate":{"$gte":0,"$lte":60}},{"geolocation":{"$near":{"$geometry":{"type":"Point","coordinates":[-122.16106,37.37646]},"$minDistance":0,"$maxDistance":10}}},{"unavailableDates":{"$nin":[]}}]}';
        const parsed = parser.parse(query);
        assert.isOk(isEqual(parsed.filter, preDefined));
    }

    @test('should parse mongo id')
    parseId() {
        const parser = new MongooseQueryParser();
        const filter: FilterQuery<any> = {
            dailyRate: { $gte: 0, $lte: 60 },
            geolocation: {
                $near: {
                    $geometry: { type: 'Point', coordinates: [-122.16106, 37.37646] },
                    $minDistance: 0,
                    $maxDistance: 10,
                },
            },
            unavailableDates: { $nin: [] },
            _org: new Types.ObjectId(),
        };

        const parsed: any = parser.parse({ filter });
        assert.isOk(isObjectIdOrHexString(parsed.filter._org));
        assert.isObject(parsed.filter);
        assert.isObject(parsed.filter.dailyRate);
    }

    @test('should use transformFilter in FilterQuery')
    transformFilter() {
        const parser = new MongooseQueryParser();
        const filter: FilterQuery<any> = {
            dailyRate: { $gte: 0, $lte: 60 },
            geolocation: {
                $near: {
                    $geometry: { type: 'Point', coordinates: [-122.16106, 37.37646] },
                    $minDistance: 0,
                    $maxDistance: 10,
                },
            },
            unavailableDates: { $nin: [] },
            _org: new Types.ObjectId().toString(),
            _org2: { $in: [new Types.ObjectId().toString()] },
        };

        const parsed: any = parser.parse({ filter });
        assert.isOk(isObjectIdOrHexString(parsed.filter._org));
        assert.isObject(parsed.filter);
        assert.isObject(parsed.filter.dailyRate);
        assert.isObject(parsed.filter._org2);

        const transformedFilter = QueryParser.transformFilter<FilterQuery<any>>(parsed);
        assert.isOk(isObjectIdOrHexString(transformedFilter.filter._org));
        assert.isOk(isObjectIdOrHexString(transformedFilter.filter._org2.$in[0]));
    }
    @test('should use transformFilter in FilterQuery with null')
    transformFilterNull() {
        const parser = new MongooseQueryParser();
        const filter: FilterQuery<any> = {
            dailyRate: { $eq: null },
        };

        const parsed = parser.parse({ filter });
        assert.isObject(parsed.filter);
    }

    @test('should use transformFilter in FilterQuery 2')
    transformFilter2() {
        const parser = new MongooseQueryParser();
        const filter: FilterQuery<any> = {
            _tags: { $in: ['66885d0e304ef564c27f5315'] },
        };
        const query = { filter, populate: 'tags' };

        const parsed = parser.parse(query);

        const transformedFilter = QueryParser.transformFilter<FilterQuery<any>>(parsed);
        assert.isOk(isObjectIdOrHexString(transformedFilter.filter._tags.$in[0]));
    }

    @test('should use transformFilter in Aggregate')
    transformFilterAggregate() {
        // let parser = new MongooseQueryParser();
        const matchStage: PipelineStage = { $match: { _id: new Types.ObjectId().toString() } };
        const matchStageIn: PipelineStage = { $match: { _id: { $in: [new Types.ObjectId().toHexString()] } } };
        const matchStageNin: PipelineStage = { $match: { _id: { $nin: [new Types.ObjectId().toHexString()] } } };
        const lookupStage: PipelineStage = {
            $lookup: { as: 'testing', from: 'testCollection', foreignField: 'foreignCollection' },
        };
        const lookupStageComplex: PipelineStage = {
            $lookup: {
                as: 'testing',
                from: 'testCollection',
                foreignField: 'foreignCollection',
                pipeline: [
                    {
                        $match: {
                            $expr: {
                                $eq: [new Types.ObjectId().toHexString()],
                            },
                        },
                    },
                    {
                        $sort: {
                            stopSequence: 1,
                        },
                    },
                    {
                        $group: {
                            _id: '$_tripTemplate',
                            firstStopTime: { $first: '$$ROOT' },
                            lastStopTime: { $last: '$$ROOT' },
                        },
                    },
                ],
            },
        };

        const transformedMatchFilter = QueryParser.transformFilter<FilterQuery<any>>(matchStage);
        assert.isOk(isObjectIdOrHexString(transformedMatchFilter.$match._id));

        const transformedMatchInFilter = QueryParser.transformFilter<FilterQuery<any>>(matchStageIn);
        assert.isOk(isObjectIdOrHexString(transformedMatchInFilter.$match._id.$in[0]));

        const transformedMatchNinFilter = QueryParser.transformFilter<FilterQuery<any>>(matchStageNin);
        assert.isOk(isObjectIdOrHexString(transformedMatchNinFilter.$match._id.$nin[0]));

        const transformLookupFilter = QueryParser.transformFilter<FilterQuery<any>>(lookupStage);
        assert.isObject(transformLookupFilter);

        const transformComplexLookupFilter = QueryParser.transformFilter<FilterQuery<any>>(lookupStageComplex);
        assert.isObject(transformComplexLookupFilter);
        assert.isArray(transformComplexLookupFilter.$lookup.pipeline);
        assert.lengthOf(transformComplexLookupFilter.$lookup.pipeline, 3);
        assert.isOk(isObjectIdOrHexString(transformComplexLookupFilter.$lookup.pipeline[0].$match.$expr.$eq[0]));
    }
}
