import { SortOrder } from 'mongoose';
export interface QueryOptions {
    filter: Object; // mongodb json query
    sort?: string | { [key: string]: SortOrder | { $meta: 'textScore' } } | [string, SortOrder][] | undefined | null; // ie.: { field: 1, field2: -1 }
    limit?: number;
    skip?: number;
    select?: string | Object; // ie.: { field: 0, field2: 0 }
    populate?: string | Object; // path(s) to populate:  a space delimited string of the path names or array like: [{path: 'field1', select: 'p1 p2'}, ...]
    deepPopulate?: string | Object; // path(s) to populate (as object):  {path:"anyPath", select:"anyField", populate:{path:"deepPath", select:"deepField"}};
    lean?: boolean;
}
