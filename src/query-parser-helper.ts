import { Model, Query, Types, Document, FilterQuery, Cursor, QueryOptions, isObjectIdOrHexString } from 'mongoose';
import { ParsedQs } from 'qs';
import { isEmpty, forOwn, isNil } from 'lodash';
import { MongooseQueryParser } from './query-parser';
import { isMongoId } from 'class-validator';
export interface IDocByQueryRes<T = Document> {
    data: T[] | T | number;
}
export interface IDocByQueryCursorRes<T = any> {
    data: Cursor<T, QueryOptions<any>>;
}

/**
 * Helper class
 * Implements the `MongooseQueryParser` and exposes a single method to execute queries on models
 */
export class QueryParser {
    /**
     * Build a Mongoose query on a given model from the parsed query string
     * @param model a mongoose `Model` object to perform the query on
     * @param query a query string object received by the controller
     * @param count an optional flag that indicates wether a `count` operation should be performed
     * @returns a Mongoose query on the provided model
     */
    static parseQuery<T = any>(model: Model<T>, query: ParsedQs, count = false): Query<any, T> {
        let docsQuery: Query<any, T, any>;
        let countQueryDocuments: Query<any, T> | null;

        docsQuery = model.find({});
        countQueryDocuments = count ? model.countDocuments() : null;

        if (!isEmpty(query)) {
            const parser = new MongooseQueryParser();
            const queryParams = parser.parse(query);
            forOwn(this.transformFilter<any>(queryParams.filter), (value, key) => {
                if (isMongoId(value)) value = Types.ObjectId.createFromHexString(value.toString());
                docsQuery = docsQuery.where(key, value);

                if (countQueryDocuments) countQueryDocuments = countQueryDocuments.where(key, value);
            });
            if (queryParams.populate) docsQuery = docsQuery.populate(queryParams.populate as any);
            if (queryParams.deepPopulate) docsQuery = docsQuery.populate(queryParams.deepPopulate as any);
            if (queryParams.sort) docsQuery = docsQuery.sort(queryParams.sort);
            if (queryParams.limit) docsQuery = docsQuery.limit(queryParams.limit);
            if (queryParams.select) docsQuery = docsQuery.select(queryParams.select as any);
            if (queryParams.skip) docsQuery = docsQuery.skip(queryParams.skip);
        }

        return count && countQueryDocuments ? countQueryDocuments : docsQuery;
    }

    /**
     * Get all documents matching the provided query for a given model
     * @deprecated The method should not be used, please use ```docsByQuery```
     * @param model a mongoose `Model` object to perform the query on
     * @param query a query string object received by the controller
     * @param count an optional flag that indicates wether a `count` operation should be performed
     * @returns the result of executing the provided query on the provided model
     */
    static async docByQuery<T = any>(model: Model<T>, query: ParsedQs, count = false): Promise<IDocByQueryRes<T>> {
        try {
            return { data: await QueryParser.parseQuery(model, query, count).exec() };
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
     * Get all documents matching the provided query for a given model
     * @param model a mongoose `Model` object to perform the query on
     * @param query a query string object received by the controller
     * @param count an optional flag that indicates wether a `count` operation should be performed
     * @returns the result of executing the provided query on the provided model
     */
    static async docsByQuery<T = any>(model: Model<T>, query: ParsedQs, count = false): Promise<IDocByQueryRes<T>> {
        try {
            return { data: await QueryParser.parseQuery(model, query, count).exec() };
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * Get all documents matching the provided query for a given model to a  mongodb cursor resource
     * @deprecated The method should not be used, please use ```docsByQueryToCursor```
     * @param model a mongoose `Model` object to perform the query on
     * @param query a query string object received by the controller
     * @param count an optional flag that indicates wether a `count` operation should be performed
     * @returns the cursor of executing the provided query on the provided model
     */
    static docByQueryToCursor<T = any>(model: Model<T>, query: ParsedQs): IDocByQueryCursorRes {
        try {
            return {
                data: QueryParser.parseQuery(model, query, false).cursor(),
            };
        } catch (error) {
            throw new Error(error);
        }
    }
    /**
     * Get all documents matching the provided query for a given model to a  mongodb cursor resource
     * @param model a mongoose `Model` object to perform the query on
     * @param query a query string object received by the controller
     * @param count an optional flag that indicates wether a `count` operation should be performed
     * @returns the cursor of executing the provided query on the provided model
     */
    static docsByQueryToCursor<T = any>(model: Model<T>, query: ParsedQs): IDocByQueryCursorRes {
        try {
            return {
                data: QueryParser.parseQuery(model, query, false).cursor(),
            };
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * Build a Mongoose `findOne` or `find` query on a given model with additional params
     * @param model a Mongoose `Model` object to perform the query on
     * @param singleMode this defines if the query should be for many or just one item
     * @param filter a Mongoose filter query on the provided model
     * @param query a query string object received by the controller
     * @returns a Mongoose query on the provided model
     */
    static parseFromCustomFilterToQuery<T = any>(
        model: Model<T>,
        singleMode: boolean,
        filter: FilterQuery<T>,
        query: ParsedQs,
    ): Query<any, T> {
        let docQuery: Query<any, any>;

        docQuery = singleMode ? model.findOne({ ...filter }) : model.find({ ...filter });

        if (!isEmpty(query)) {
            const parser = new MongooseQueryParser({
                blacklist: ['filter', 'sort', 'limit', 'skip'],
            });
            const queryParams = parser.parse(query);
            if (queryParams.populate) docQuery = docQuery.populate(queryParams.populate as any);
            if (queryParams.deepPopulate) docQuery = docQuery.populate(queryParams.deepPopulate as any);
            if (queryParams.select) docQuery = docQuery.select(queryParams.select as any);
        }

        return docQuery;
    }
    /**
     * Build a Mongoose `findOne` query on a given model with additional params
     * @param model a Mongoose `Model` object to perform the query on
     * @param filter a Mongoose filter query on the provided model
     * @param query a query string object received by the controller
     * @returns a Mongoose query on the provided model
     */
    static parseFilterQuery<T = any>(model: Model<T>, filter: FilterQuery<T>, query: ParsedQs): Query<any, T> {
        let docQuery: Query<any, any>;

        docQuery = model.findOne({ ...filter });

        if (!isEmpty(query)) {
            const parser = new MongooseQueryParser({
                blacklist: ['filter', 'sort', 'limit', 'skip'],
            });
            const queryParams = parser.parse(query);
            if (queryParams.populate) docQuery = docQuery.populate(queryParams.populate as any);
            if (queryParams.deepPopulate) docQuery = docQuery.populate(queryParams.deepPopulate as any);
            if (queryParams.select) docQuery = docQuery.select(queryParams.select as any);
        }

        return docQuery;
    }

    /**
     * Get a document matching the provided query for a given model
     * @param model a Mongoose `Model` object to perform the query on
     * @param filter a Mongoose filter query on the provided model
     * @param query a query string object received by the controller
     * @returns the result of executing the provided query on the provided model
     */
    static async docByFilter<T = any>(
        model: Model<T>,
        filter: FilterQuery<T>,
        query: ParsedQs,
    ): Promise<IDocByQueryRes<T>> {
        try {
            return {
                data: await QueryParser.parseFilterQuery(model, filter, query).exec(),
            };
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * Get a document matching the provided query for a given model
     * @param model a Mongoose `Model` object to perform the query on
     * @param singleMode this defines if the query should be for many or just one item
     * @param filter a Mongoose filter query on the provided model
     * @param query a query string object received by the controller
     * @returns the result of executing the provided query on the provided model
     */
    static docsFromCustomFilterToCursor<T = any>(
        model: Model<T>,
        singleMode: boolean,
        filter: FilterQuery<T>,
        query: ParsedQs,
    ): IDocByQueryCursorRes<T> {
        try {
            return {
                data: QueryParser.parseFromCustomFilterToQuery(model, singleMode, filter, query).cursor(),
            };
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * Get a document matching the provided query for a given model
     * @param model a Mongoose `Model` object to perform the query on
     * @param singleMode this defines if the query should be for many or just one item
     * @param filter a Mongoose filter query on the provided model
     * @param query a query string object received by the controller
     * @returns the result of executing the provided query on the provided model
     */
    static async docsByFilter<T = any>(
        model: Model<T>,
        singleMode: boolean,
        filter: FilterQuery<T>,
        query: ParsedQs,
    ): Promise<IDocByQueryRes<T>> {
        try {
            return {
                data: await QueryParser.parseFromCustomFilterToQuery(model, singleMode, filter, query).exec(),
            };
        } catch (error) {
            throw new Error(error);
        }
    }

    /**
     * Build a Mongoose `findOneById` query on a given model with additional params
     * @param model a Mongoose `Model` object to perform the query on
     * @param _id the resource id
     * @param query a query string object received by the controller
     * @returns a Mongoose query on the provided model
     */
    static parseByIdQuery<T = any>(model: Model<T>, _id: string | Types.ObjectId, query: ParsedQs): Query<any, T> {
        return QueryParser.parseFilterQuery(model, { _id }, query);
    }

    /**
     * Get a document matching the provided query for a given model
     * @param model a Mongoose `Model` object to perform the query on
     * @param _id the resource id
     * @param query a query string object received by the controller
     * @returns the result of executing the provided query on the provided model
     */
    static docById<T = any>(model: Model<T>, _id: string, query: ParsedQs): Promise<IDocByQueryRes<T>> {
        return QueryParser.docByFilter(model, { _id }, query);
    }

    /**
     * Transforms the filter param to correctly support `ObjectId`s
     * @param filter the filter param
     * @returns a casted filter
     */
    static transformFilter<T = any>(filter: T): T {
        const walk = (obj: any) => {
            for (const [i, [key, value]] of Object.entries(obj).entries()) {
                if (Object.prototype.hasOwnProperty.call(obj, key)) {
                    const validId = isObjectIdOrHexString(value);
                    // if (validId && isMongoId(value)) {
                    //     obj[key] = Types.ObjectId.createFromHexString(value.toString());
                    //     // console.log('value :>> ', obj[key]);
                    //     console.log('valid', value);
                    // }
                    // if (typeof value === 'object' && !validId) {
                    //     console.log('walking', value);
                    //     if (i % 1000 === 0) {
                    //         setImmediate(() => {
                    //             walk(obj[key]);
                    //         });
                    //     } else walk(obj[key]);
                    // } else {
                    //     console.log('value as is ', value);
                    //     obj[key] = value;
                    // }
                    // console.log(obj);
                    // console.log('HERE', key);
                    // console.log('obj :>> ', obj);
                    if (typeof value === 'object' && !validId && !isNil(value)) {
                        // console.log('walking......', key);
                        walk(obj[key]);
                    } else obj[key] = validId ? Types.ObjectId.createFromHexString(value.toString()) : value;
                }
            }
            return obj;
        };
        const res = walk({ ...filter });
        // console.log('res', res.filter);
        return res;
    }
}
